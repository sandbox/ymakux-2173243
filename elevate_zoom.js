(function ($) {
  Drupal.behaviors.elevateZoom = {
    attach: function (context, settings) {
      $('#elevate-zoom-wrapper').toggleClass('nojs js');
      $("#elevate-zoom").elevateZoom(settings.elevateZoom);
    }
  }
})(jQuery);
