<div id="elevate-zoom-wrapper" class="nojs">
  <img id="elevate-zoom" data-zoom-image="<?php print $main_image['url']['original']; ?>" src="<?php print $main_image['url']['medium']; ?>" />
  <?php if($thumbs): ?>
    <div id="elevate-zoom-gallery">
      <?php foreach ($thumbs as $delta => $thumb): ?>
      <a href="#" data-image="<?php print $thumb['url']['medium']; ?>" data-zoom-image="<?php print $thumb['url']['original']; ?>">
        <img id="img_<?php print $delta; ?>" src="<?php print $thumb['url']['thumb']; ?>" />
      </a>
      <?php endforeach; ?>
    </div>
  <?php endif; ?>
</div>